from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest

class Story8Test(unittest.TestCase):
	def setUp(self):
		self.browser = webdriver.Firefox()

	def tearDown(self):
		self.browser.quit()

	def test_check_title_correct(self):
		self.browser.get('http://story8lucky.herokuapp.com/')
		self.assertIn('Story 8', self.browser.title)

	def test_header_correct(self):
		self.browser.get('http://story8lucky.herokuapp.com/')
		header_text = self.browser.find_element_by_tag_name('h2').text
		self.assertIn(header_text, "Accordions")

	def test_accordion_can_collapse_down(self):
		self.browser.get('http://story8lucky.herokuapp.com/')
		accordion1 = self.browser.find_element_by_id("Demo1")
		accordion1.click()
		self.assertIn("w3-show", accordion1.getAttribute("class").split(" "))

if __name__ == '__main__':  
	unittest.main(warnings='ignore') 
	